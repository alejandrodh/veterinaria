import time

#*************CLASES***************CLASES******************CLASES*******************CLASES************CLASES*************************

class Cliente(object):
    """
    representa los datos de una persona
    atributos:
        cedula: cedula de la persona
        nombre: nombre de la persona
        genero: genero de la persona
        estado: divorciado, soltero, casado
        telefono: telefono de la persona
        correo: correo de la persona
        localidad: lugar donde vive

    """
    def __init__(self,cedula,nombre,genero,telefono1,telefono2,correo,localidad):

         self.cedula= cedula
         self.nombre=nombre
         self.genero = genero
         self.telefono1 = telefono1
         self.telefono2 = telefono2
         self.correo = correo
         self.localidad= localidad


    def __str__(self):
        return ("Cedula: {0}\n" 
                "Nombre: {1}\n"
                "Genero: {2}\n"
                "Telefono: {3}\n"
                "Telefono Alternativo: {4}\n"
                "Correo: {5}\n"
                "Dirrecciòn: {6}".format(self.cedula, self.nombre, self.genero, self.telefono1, self.telefono2,
                                         self.correo,self.localidad))


class Mascota(object):
    """
        representa los datos de una mascota
        atributos:
             code: codigo de la mascota
             name: nombre de la mascota
             f_de_i: fecha de ingreso de la mascota
             f_de_n: fecha de nacimiento de la mascota
             sexo: genero de la mascota
             tipo: tipo de la mascota
             raza: raza de la mascota
             color: color de la mascota
             peso: peso de la mascota
             cedula: cedula del dueño con todos los datos
             observaciones: observaciones de la mascota


    """
    def __init__(self, code, name, f_de_i, f_de_n, sexo, tipo, raza, color, peso,cedula,observaciones):
        self.code = code
        self.name = name
        self.f_de_i = f_de_i
        self.f_de_n = f_de_n
        self.sexo = sexo
        self.tipo = tipo
        self.raza = raza
        self.color = color
        self.peso = peso
        self.cedula = cedula
        self.observaciones = observaciones

    def __str__(self):
        return ("Codigo: {0} \n"
                "Nombre: {1} \n"
                "Fecha de Ingreso: {2}\n"
                "Fecha de Nacimiento: {3}\n"
                "Sexo: {4}\n"
                "Tipo: {5}\n"
                "Raza: {6}\n"
                "Color: {7}\n"
                "Peso: {8}\n"
                "Cedula de el dueño: {9}\n"
                "Observaciones: {10}\n".format (self.code, self.name, self.f_de_i,self.f_de_n,self.sexo,self.tipo,
                                            self.raza,self.color,self.peso,self.cedula,self.observaciones))


class Vacuna(object):
    """
        representa los datos de una vacuna
        atributos:
             cod_v: codigo de la vacuna
             nom: nombre de la vacuna
             des: descripcion de la vacuna
             precio_v: precio de la vacuna
    """
    def __init__(self,cod_v,nom,des,precio_v):
        self.cod_v = cod_v
        self.nom = nom
        self.des = des
        self.precio_v = precio_v

    def __str__(self):
        return ("Codigo: {0}\n"
                "Nombre: {1}\n"
                "Descripcion: {2}\n Precio: {3}\n".format(self.cod_v,self.nom,self.des,self.precio_v))

class Tratamiento(object):
    """
        representa los datos de un tratamiento
        atributos:
              cod_t: codigo del tratamiento
              nom: nombre del tratamiento
              des: descripcion del tratamiento
              precio_t: precio del tratamiento

    """
    def __init__(self, cod_t, nom, des, precio_t):
        self.cod_t = cod_t
        self.nom = nom
        self.des = des
        self.precio_t = precio_t

    def __str__(self):
        return ("Codigo: {0}\n"
                "Nombre: {1}\n"
                "Descripcion: {2}\n"
                "Precio: {3}\n".format(self.cod_t, self.nom, self.des, self.precio_t))


class Servicio(object):
    """
        representa los datos del servicio
        atributos:
              cod_s: codigo del servicio
              nom: nombre del servicio
              des: descripcion del servicio
              precio_s: precio del servicio
    """
    def __init__(self, cod_s, nom, des, precio_s):
        self.cod_s = cod_s
        self.nom = nom
        self.des = des
        self.precio_s = precio_s

    def __str__(self):
        return ("Codigo: {0}\n"
                "Nombre: {1}\n"
                "Descripcion: {2}\n"
                "Precio: {3}\n".format(self.cod_s, self.nom, self.des, self.precio_s))

class Control(object):
    """
        representa los datos del control de la mascota
        atributos:
               cod_c: codigo del control
               cod_m: codigo de la mascota
               cod_v: codigo de las vacunas
    """
    def __init__(self,cod_c,cod_m,cod_v, f_aplicacion, observaciones):
        self.cod_c = cod_c
        self.cod_m = cod_m
        self.cod_v = cod_v
        self.f_aplicacion = f_aplicacion
        self.observaciones = observaciones

    def __str__(self):
        return ("Codigo del Control: {0}\n"
                "Codigo de la mascota: {1}\n"
                "Codigo de la vacuna: {2}\n"
                "Fecha de aplicacion: {3}\n"
                "Observaciones: {4}\n".format(self.cod_c,self.cod_m,self.cod_v,self.f_aplicacion,
                                             self.observaciones))


class Historial(object):
    """
        representa los datos del historial
        atributos:
              cod_h: codigo del historial
              cod_m: codigo de la mascota
              peso: peso de la mascota
              temp: temperatura de la mascota
              tratamiento: tratamiento de la mascota
              observa: observaciones de la mascota
    """
    def __init__(self,cod_h,cod_m,peso,temperatura,tratamiento,observaciones,antecedentes,fecha):
        self.cod_h = cod_h
        self.cod_m = cod_m
        self.peso = peso
        self.temp = temperatura
        self.tratamiento = tratamiento
        self.observa = observaciones
        self.ante = antecedentes
        self.fecha = fecha

    def __str__(self):
        return ("Codigo: {0}\n"
                "Codigo de la mascota: {1}"
                "Peso: {2}\n"
                "Temperatura: {3}\n"
                "Tratamiento:{4}\n"
                "Observaciones: {5}\n"
                "Antecedentes: {6}\n"
                "fecha: {7}\n".format(self.cod_h, self.cod_m,self.peso,self.temp,self.tratamiento,self.observa,
                                             self.ante,self.fecha))


class Servicios(object):
    def __init__(self,cedula,num_servi,descripcion,id_mascota,fecha):
        self.cedula = cedula
        self.num_servi = num_servi
        self.descripcion = descripcion
        self.id_mascota = id_mascota
        self.fecha = fecha

    def __str__(self):
        return ("cedula del cliente: {1}\n"
                "num_servi: {2}\n"
                "descripcion: {3}\n"
                "id_mascota: {4}\n"
                "fecha: {5}\n").format(self.cedula,self.num_servi,self.descripcion,self.id_mascota,self.fecha)


class Usuario(object):
    """
        representa los datos de los usuarios
        atributos:
             usuario: nombre de usuario
             contraseña: contraseña del nombre de usuario
    """
    def __init__(self,usuario,contra):
        self.usuario = usuario
        self.contra = contra

    def __str__(self):
        return ("usuario: {0}\n"
                "contraseña: {1}\n".format(self.usuario, self.contra))



#**********LISTAS**********DICCIONARIOS************LISTAS************DICCIONARIOS************LISTAS*********DICCIONARIOS***********

usuario1 = Usuario("admin","admin")

usuarios = {usuario1.usuario:usuario1}

cliente1 = Cliente(207870636,"alejandro","masculino",111111,222222,"mamahotmal.com","cq")

clientes = {cliente1.cedula:cliente1}
l_cliente = [clientes.keys()]

mascota1 = Mascota("P-001", "pepe","10/10/2018","5/4/2010","masculino","perro","pincher","negro",50,cliente1.cedula,
                   "el perro esta obeso")
mascota2 = Mascota("v-001","lula","16/8/2018","5/12/2005","femenina","vaca","Angus","blanca",450,cliente1.cedula,
                   "la vaca parece cansada")
mascotas = {mascota1.code: mascota1,mascota2.code:mascota2}
l_mascota = [mascotas.keys()]

t_mascotas = ["Perro","Hamster","Gato","Caballo","Vaca"]
r_perros = ["Pug", "Bóxer","Dóberman","Bulldog","Pastor Alemán","Stanford","Pitbull","Rootweiler","Bullterrier","Pomeranian"]
r_gatos = ["Persa", "Siamés","Abisio","Vengala","Siberiano","Angora","Somalí","Burmilla","Orinetal","Bombay"]
r_hamster = ["Enano Ruso","Roborovski","Dorado","Enano de Campbell","arlequín","angora","Enano de China","albino","Panda"]
r_caballos = ["Mustang","Pura Sangre","Appaloosa","Islándes","Frison","Morgan","Cuarto de milla","Percherón","Criollo","Español"]
r_vacas = ["Jersey","Holstein","Simental","Angus","Brahman","Gyr","Hereford","Charolais","Marchigiana","Romagnola"]
#probando
razas = {"Perro": r_perros,"Gato": r_gatos,"hamster":r_hamster,"caballo": r_caballos,"vacas": r_vacas}

vac1 = Vacuna("v-001","Vacuna antirrábica (HDCV)","Para control de la enfermedad de la rabia",20000)
vac2 = Vacuna("v-002","BRUCELLA ABORTUS RB51® BECERRAS","Vacuna contra la brucelosis bovina.",45000)
vac3 = Vacuna("v-003", "Octovalente", "inmuniza contra moquillo", 15000)
vacunas = {vac1.cod_v: vac1,vac2.cod_v:vac2,vac3.cod_v:vac3}
l_vacunas = [vacunas.keys()]

servi1 = Servicio("s-001","Peluqueria","cortarle el pelo a las mascotas",7000)
servi2 = Servicio("s-002","Baño","Baña al animal",5000)
servi3 = Servicio("s-003","limpieza dental","revisar y mejorar la salud vocal del animal",10000)
servicios = {servi1.cod_s:servi1,servi2.cod_s:servi2,servi3.cod_s:servi3}
l_servis = [servicios.keys()]

trata1 = Tratamiento("t-001","Castración","la operacion para que los animales no tengan mas crias","25000")
trata2 = Tratamiento("t-002","nutricionista","revisa la dieta del animal",15000)
trata3 = Tratamiento("t-003","oftalmologia","revisa la salud ocular dell animal",15000)
tratamientos = {trata1.cod_t:trata1,trata2.cod_t:trata2,trata3.cod_t:trata3}
l_tratamientos = [tratamientos.keys()]

l_todo = [l_tratamientos,
          l_servis,
          l_vacunas,]



control1 = Control("c-001",mascota1.code,vac1.cod_v,"19/7/2017","esta muriendo")
controles = {control1.cod_c:control1}

historial1 = Historial("h-001",mascota1.code,30,39,trata1.cod_t,"esta debil","n/a",("23/8/2018"))
historiales = {historial1.cod_h:historial1}

#**********DEF************DEF***************DEF******************DEF****************DEF**********DEF********DEF**************

def inicio(usua,password):
    """
        sirve para iniciar sesion
        :param usua: nombre de usuario
        :param password: contraseña del nombre de usuario
        :return:
    """
    if usua in usuarios:
        if password == usuarios[usua].contra:
            return 1
        else:
            return "contraseña invalida, intentelo nuevamente"
    else:
        return "usuario no valido, intentelo nuevamente"

def registrar(usuario1):
    """
       sirve para registrar usuarios
       :param usuario1: registra el usuario
       :return:
       """

    if usuario1.usuario in usuarios:
        return "este usuario ya existe"
    else:
        usuarios[usuario1.usuario] = usuario1
        return "se ha registrado correctamente el usuario"


def r_mascotas(mascota1):
    """
        registra a una mascota
        :param mascota1: registra los datos de una mascota
        :return:
        """
    if  mascota1.code in mascotas:
        return "esta mascota ya exite"
    else:
        mascotas[mascota1.code] = mascota1
        return "se ha agregado la mascota"



def r_cliente(cliente1):
    """
        registra un cliente
        :param cliente1: registra los datos de un cliente
        :return:
        """
    info = "ese cliente ya se encuentra registrado"
    if not cliente1.cedula in clientes:
        clientes[cliente1.cedula] = cliente1
        info = "Se ha registrado a el cliente exitosamente"
    return info


def l_tipo():
    """
        imprime la lista de los tipos de mascotas
        :return:
        """
    info = "Tipos de Mascotas\n"
    num = 1
    for tipo in razas.keys():
        if len(tipo) < 10:
            tipo = tipo + " " * (10 - len(tipo))

        if num % 2 == 0:
            info += "{0}- {1}\n".format(num, tipo)
        else:
            info += "{0}- {1}\t\t".format(num, tipo)
        num += 1
    return info


def l_raza (tipo):
    """
    imprime la lista de razas
        :param tipo: raza de la mascota
        :return:
        """
    info = "Lista de Razas\n"
    num = 1

    if tipo == "Perro":
        for raza in r_perros:
            if len(raza) < 10:
                raza = raza + " " * (10 - len(raza))

            if num % 2 == 0:
                info += "{0}- {1}\n".format(num, raza)
            else:
                info += "{0}- {1}\t\t".format(num, raza)
            num += 1

    elif tipo == "Hamster":
        for raza in r_hamster:
            if len(raza) < 10:
                raza = raza + " " * (10 - len(raza))

            if num % 2 == 0:
                info += "{0}- {1}\n".format(num, raza)
            else:
                info += "{0}- {1}\t\t".format(num, raza)
            num += 1

    elif tipo == "Gato" :
        for raza in r_gatos:
            if len(raza) < 10:
                raza = raza + " " * (10 - len(raza))

            if num % 2 == 0:
                info += "{0}- {1}\n".format(num, raza)
            else:
                info += "{0}- {1}\t\t".format(num, raza)
            num += 1

    elif tipo == "Caballo" :
        for raza in r_caballos:
            if len(raza) < 10:
                raza = raza + " " * (10 - len(raza))

            if num % 2 == 0:
                info += "{0}- {1}\n".format(num, raza)
            else:
                info += "{0}- {1}\t\t".format(num, raza)
            num += 1

    elif tipo == "Vaca":
        for raza in r_vacas:
            if len(raza) < 10:
                raza = raza + " " * (10 - len(raza))

            if num % 2 == 0:
                info += "{0}- {1}\n".format(num, raza)
            else:
                info += "{0}- {1}\t\t".format(num, raza)
            num += 1
    return info

def l_clientes():
    """
    imprime la lista de los clientes registrados
    :return:
    """
    info = "Clientes existentes\n"
    num = 1
    for cliente in clientes:
        info += "{0}- {1}\n".format(num,cliente)
        num += 1
    return info

def l_mascotas():
    """
    imprime la lista de las mascotas registradas
    :return:
    """
    info = "Mascotas existentes\n"
    num = 1
    for mascota in mascotas:
        info += "{0}- {1}\n".format(num,mascota)
        num += 1
    return info

def l_vacunas():
    """
    imprime la lista de vacunas registradas
    :return:
    """
    info = "Vacunas existentes\n"
    num = 1
    for vacuna in vacunas:
        info += "{0}-{1}\n".format(num,vacuna)
        num += 1
    return info

def l_tratamintos():
    """
    imprime la lista de tratamientos registrados
    :return:
    """
    info = "tratamientos existentes\n"
    num = 1
    for trata in tratamientos:
        info += "{0}-{1}\n".format(num,trata)
        num += 1
    return info


def l_servicios():
    """
    imprime la lista de servicios registrados
    :return:
    """
    info = "servicios existentes\n"
    num = 1
    for servi in servicios:
        info += "{0}-{1}\n".format(num,servi)
        num += 1
    return info

def lt_servicios():
    """
    imprime una lista de todos los servicios registrados
    :return:
    """
    info = "Servicios disponibles"
    num = 1
    for servicio in l_todo:
        info += "{0}-{1}\n".format(num,servicio)
        num += 1
    return info


def s_raza (raza):
    """
        sirve para seleccionar la raza de la mascota
        :param raza: raza registrada
        :return:
        """
    if tip == "Perro":
        if raza > len(r_perros):
            return "la opcion seleccionada no es valida"
        else:
            return r_perros[raza - 1]

    elif tip == "Hamster":
        if raza > len(r_hamster):
            return "la opcion seleccionada no es valida"
        else:
            return r_hamster[raza - 1]

    elif tip == "Gato":
        if raza > len(r_gatos):
            return "la opcion seleccionada no es valida"
        else:
            return r_gatos[raza - 1]
    elif tip == "Caballo":
        if raza > len(r_caballos):
            return "la opcion seleccionada no es valida"
        else:
            return r_caballos[raza - 1]
    elif tip == "Vaca":
        if raza > len(r_vacas):
            return "la opcion seleccionada no es valida"
        else:
            return r_vacas[raza - 1]
    return "se ha selecciona una raza"


def s_tipo(tipo):
    """
        sirve para seleccionar
        :param tipo:
        :return:
        """
    if tipo > len(t_mascotas):
         return "opcion no valida"
    else:
         return t_mascotas[tipo - 1]


def s_historial(x,historial1):
    """
    sirve para seleccionar un historial
    :param x: variable de almacenamiento
    :param historial1: historial registrado
    :return:
    """
    if x in historiales:
        lista = historiales[x]
        lista.append(historial1) # WTF
        historiales[x] = lista
        return "Se añadio al historial"
    else:
        historiales[x] = [historial1]
        return "Se agrego el historial"


def control_v(o,control1):
    """
        sirve para el control de las vacunas
        :param o: variable de almacenamiento
        :param control1: control registrado
        :return:
        """
    if o in controles:
        lista = controles[o]
        lista.append(control1)
        controles[o] = lista
        return "Se modifico el control"
    else:
        controles[o] = [control1]
        return "se agrego el control a la lista de controles"


def s_mascotas(s_mascota):
    """
    sirve para seleccionar a una mascota
    :param s_mascota: variable de almacenamiento
    :return:
    """
    if s_mascota > len(l_mascota):
        return "esa opcion no es valida"
    else:
       return l_mascota[s_mascota - 1]


def s_cliente(cliente):
    """
    sirve para seleccionar el cliente
    :param cliente: clientes registrado
    :return:
    """
    if cliente > len(clientes):
        return "opcion no valida"
    else:
        lista = list(clientes.keys())
        ced = lista[cliente - 1]
        return clientes[ced]

def s_vacuna(s_vacuna):
    """
    sirve para seleccionar una vacuna
    :param vacuna: vacunas registradas
    :return:
    """
    if s_vacuna > len(vacunas):
        return "opcion no valida"
    else:
        lista = list(vacunas.keys())
        cod = lista[vacuna - 1]
        return vacunas[cod]

def s_tratamiento(tratamiento):
    """
    sirve para seleccionar un tratamiento
    :param tratamiento: tratamiento registrado
    :return:
    """
    if tratamiento > len(tratamientos):
        return "opcion no valida"
    else:
        lista = list(tratamientos.keys())
        cod = lista[tratamiento - 1]
        return tratamientos[cod]

def s_servicio(servicio):
    """
    sirve para seleccionar un servicio
    :param servicio: servicio registrado
    :return:
    """
    if servicio > len(servicios):
        return "opcion no valida"
    else:
        lista = list(servicios.keys())
        cod = lista[servicio - 1]
        return servicios[cod]


def c_mascotas(mascota):
    """
    sirve para consultar una mascota
    :param mascota: mascota registrada
    :return:
    """
    if mascota > len(mascotas):
        return "opcion no valida"
    else:
        lista = list(mascotas.keys())
        code = lista[mascota - 1]
        return mascotas[code]


def c_vacunas(vacuna):
    """
    sirve para llevar el control de las vacunas
    :param vacuna: vacunas registradas
    :return:
    """
    if vacuna > len(vacunas):
        return "opcion no valida"
    else:
        lista = list(vacunas.keys())
        code = lista[vacuna - 1]
        return vacunas[code]


def c_tratamientos(tratamiento):
    """
    sirve para llevar el control de los tratamientos
    :param tratamiento: tratamientos registrados
    :return:
    """
    if tratamiento > len(tratamientos):
        return "opcion no valida"
    else:
        lista = list(tratamientos.keys())
        code = lista[tratamiento - 1]
        return tratamientos[code]


def c_servicios(servicio):
    """
    sirve para llevar el control de los servicios
    :param servicio: servicios registrados
    :return:
    """
    if servicio > len(servicios):
        return "opcion no valida"
    else:
        lista = list(servicios.keys())
        code = lista[servicio - 1]
        return servicios[code]


def v_historial(x):
    """
    sirve para ver el historial
    :param x: variable de almacenamiento del for
    :return:
    """
    info = ""
    if x in historiales:
        for x in historiales:
            info += "{0}\n".format(historiales[x])
    return info


def v_control(i):
    """
    sirve para ver el control
    :param i: variable de almacenamiento del for
    :return:
    """
    info = ""
    if i in controles:
        for i in controles:
            info += "{0}\n".format(controles[i])
    return info


def consultar_cod_m(p_cod_m):
    """
    sirve para consultar el codigo de la mascota
    :param p_cod_m: codigo de la mascota
    :return:
    """
    if p_cod_m > len(l_mascota):
        return "ese codigo de mascota no existe"
    else:
        return l_mascota[p_cod_m - 1]


def consultar_l_todo(p_l_todo):
    """
    sirve para consultar la lista de todos los tratamientos
    :param p_l_todo: lista de todos los tratamientos
    :return:
    """
    if p_l_todo > len(l_todo):
        return "opcion invalida,intente otra vez"
    else:
        return l_todo[p_l_todo -1]


def agregar_v(cod_v, nom, des, precio_v):
    """
    sirve para registrar una vacuna al sistema
    :param cod_v: codigo de la vacuna
    :param nom: nombre de la vacuna
    :param des: descripcion de la vacuna
    :param precio_v: precio de la vacuna
    :return:
    """
    info = "La vacuna que esta intentando ingresar ya esta en el sistema"
    if not cod_v in vacunas:
         v = Vacuna(cod_v, nom, des, precio_v)
         vacunas[v.cod_v] = v
         info = "Registrada con exito"
    return info


def agregar_t(cod_t, nom, des, precio_t):
    """
    sirve para agregar un tratamiento al sistema
    :param cod_t: codigo del tratamiento
    :param nom: nombre del tratamiento
    :param des: descripcion del tratamiento
    :param precio_t: precio del tratamiento
    :return:
    """
    info = "La vacuna que esta intentando ingresar ya esta en el sistema"
    if not cod_t in tratamientos:
        t = Tratamiento(cod_t, nom, des, precio_t)
        tratamientos[t.cod_t] = t
        info = "Registrada con exito"
    return info


def agregar_s(cod_s, nom, des, precio_s):
    """
    sirve para agragar un servicio al sistema
    :param cod_s: codigo del servicio
    :param nom: nombre del servvicio
    :param des: descripcion del servicio
    :param precio_s: precio del servicio
    :return:
    """
    info = "La vacuna que esta intentando ingresar ya esta en el sistema"
    if not cod_s in servicios:
        s = Servicio(cod_s, nom, des, precio_s)
        servicios[s.cod_s] = s
        info = "Registrada con exito"
    return info


def eliminar_v(codigo):
    """
    sirve para eliminar una vacuna
    :param codigo: codigo de la vacuna
    :return:
    """
    info = "el codigo digitado no existe en el sistema"
    if codigo in vacunas:
        del vacunas[codigo]
        info = "se ha eliminado la vacuna deseada"
    return info


def eliminar_t(codigo):
    """
    sirve para eliminar un tratamiento
    :param codigo: codigo del tratamiento
    :return:
    """
    info = "el codigo digitado no existe en el sistema"
    if codigo in tratamientos:
        del tratamientos[codigo]
        info = "se ha eliminado el tratamiento deseada"
    return info


def eliminar_s(codigo):
    """
    sirve para eliminar un servicio
    :param codigo: ccodigo del servicio
    :return:
    """
    info = "el codigo digitado no existe en el sistema"
    if codigo in servicios:
        del servicios[codigo]
        info = "se ha eliminado el servicio deseada"
    return info


def e_tipo(codigo):
    """
    sirve para eliminar un tipo
    :param codigo: codigo del tipo
    :return:
    """
    info = "El tipo de animal no existe en el sistema"
    if codigo in t_mascotas:
        t_mascotas.remove(codigo)
        info = "Se ha eliminado ",codigo," del sistema"
    return info


def e_raza(tip,codigo):
    """
    sirve para eliminar una raza
    :param codigo: codigo de la raza
    :return:
    """
    info = "La Raza selecciona no existe en el sistema"
    if tip in razas.keys():
        print("ESTING BORRANDO: ", codigo, " de la lista: ", razas[tip])
        l_temp_raza = razas[tip]
        l_temp_raza.remove(codigo)

        info = "Se ha eliminado",codigo,"de la raza de",l_temp_raza
    return info


def c_mascota(mascota,t_mascotas):
    """
    sirve para consultar una mascota
    :param mascota: mascotas a registrar y registradas
    :return:
    """
    info = "el tipo ya esta registrado"
    if mascota in t_mascotas:
        l_temp_tipos = t_mascotas[mascota]
        l_temp_tipos.append(t_mascotas)
        info = "El nuevo tipo de mascota a sido registrada con exito"
    return info

def c_raza(raza,tip):
    """
    sirve para consultar una raza
    :param raza: raza de la mascota
    :param tip: tipo de la mascota
    :return:
    """
    print("probando",tip)
    info = "La raza ya esta registrada"
    if tip in razas.keys():
        l_temp_razas = razas[tip]
        l_temp_razas.append(raza)
        info = "La nueva raza a sido agregada con exito"
    return info

#************MENU************MENU**************MENU********************MENU********************MENU****************MENU***********

menu_log = ("******Sistema de Manejo de Veterinaria******\n"
            "1.Iniciar Sesion\n"
            "2.Cerrar\n"
            "Por favor, seleccione una opcion: ")

main_menu = ("\n******----------> BIENVENIDO AL MENU DE LA VETERINARIA <----------******\n"
             "1.Crear Nuevo Usuario\n"
             "2.Registrar Cliente\n"
             "3.Registrar Mascota\n"
             "4.Nuevo Ingreso al Sistema(vacuna, tratamiento, servicio, etc)\n"
             "5.Historial Clinico\n"
             "6.Hoja de vacunas\n"
             "7.Consultar Registros\n"
             "8.Servicios\n"
             "9.Pagos pendientes\n"
             "10.Cerrar sesion\n"
             "Por favor, seleccione una opcion: ")

menu_ingreso =("\n******-----> Nuevo Ingreso al Sistema <------******\n"
               "1.Vacuna\n"
               "2.Tratamiento\n"
               "3.Servicio\n"
               "4.Tipos de animales\n"
               "5.Razas\n"
               "6.Volver al Menu Principal\n"
               "Por Favor, Seleccione una Opcion: ")

sub_menu_ingreso = ("\n"
                    "1.Agregar\n"
                    "2.Eliminar\n"
                    "3.Volver\n"
                    "Por favor, Seleccione una Opcion: ")

sub_menu_historial = ("\n"
                      "1.Crear\n"
                      "2.Consultar Historial\n"
                      "3.Volver\n"
                      "Por favor, Seleccione una Opcion: ")

sub_menu_control = ("\n"
                    "1.Crear\n"
                    "2.Consultar control\n"
                    "3.Volver\n"
                    "Por favor, Seleccione una Opcion: ")

sub_menu_registros = ("\n"
                      "1.Clientes\n"
                      "2.Mascotas\n"
                      "3.Vacunas\n"
                      "4.Tratamientos\n"
                      "5.Servicios\n"
                      "6.Volver\n"
                      "Por favor, Seleccione un opcion: ")

sub_menu_servicios = ("\n"
                     "1.peluqueria\n"
                     "2.baño\n"
                     "3.limpieza dental\n"
                     "4.volver\n"
                     "seleccione una opcion: ")

while True:
    op = int(input(menu_log))

    if op == 2:
        print("-----> Cerrando Programa <------")
        break
    elif op == 1:
        usua = input("Digite su usuario: ")
        password = input("Digite su contraseña: ")
        if inicio(usua,password) == 1:
            print("-----> Contraseña correcta <-------\n"
                  "Bienvenido",usua)
            while True:
                op = int(input(main_menu))

                if op == 10:
                    break

                elif op == 1:
                    n_usuario = input("Digite el nombre del nuevo usuario: ")
                    n_contra = input("Digite la contraseña del usuario nuevo: ")
                    usuario1 = Usuario(n_usuario,n_contra)
                    print(registrar(usuario1))

                elif op == 2:
                    cedula = int(input("Digite la el ID(ej:207920992): "))
                    nombre = input("Digite su nombre completo: ")
                    genero = input("Digite su genero: ")
                    telefono1 = input("Digite su numero de telefono: ")
                    telefono2 = input("Digite su numero de telefono alternativo: ")
                    email = input("Digite su email: ")
                    localidad = input("Digite su direccion: ")
                    cliente1 = Cliente(cedula, nombre, genero, telefono1, telefono2, email, localidad)
                    print(r_cliente(cliente1))

                elif op == 3:
                   code = input("Digite el codigo de la mascota: ")
                   name = input("Digite el nombre de la mascota: ")
                   f_de_i = input("Digite la fecha de ingreso: ")
                   f_de_n = input("DIgite la fecha de nacimiento: ")
                   sexo = input("Digite el genero de la mascota: ")
                   print(l_tipo())
                   tipo = int(input("Seleccione el Tipo de Mascota: "))
                   print(s_tipo(tipo))
                   tip = s_tipo(tipo)
                   print(l_raza(tip))
                   raza = int(input("Seleccione la raza de la mascota: "))
                   print(s_raza(raza))
                   raz = s_raza(raza)
                   color = input("Digite el color de la mascota: ")
                   peso = input("Digite el peso de la mascota en kg: ")
                   print(l_clientes())
                   cliente = int(input("Seleccione el cliente al que pertenese la mascota: "))
                   client = s_cliente(cliente)
                   print(client)
                   observaciones = input("Ingrese las observaciones realizadas a la mascota: ")
                   mascota1 = Mascota(code,name,f_de_i,f_de_n,sexo,tip,raz,color,peso, client.cedula,observaciones)
                   print(r_mascotas(mascota1))

                elif op == 4:
                    while True:
                        op = int(input(menu_ingreso))

                        if op == 6:
                            break
                        elif op == 1: #vacunas cod_v,nom,des,precio_v
                            while True:
                                op = int(input(sub_menu_ingreso))

                                if op == 3:
                                    break
                                elif op == 1:
                                    cod_v = input("Digite el codigo de la vacuna nueva: ")
                                    nom = input("Digite el nombre de la vacuna nueva: ")
                                    des = input("Digite una descripcion: ")
                                    precio_v = input("Digite el precio de la vacuna nueva: ")
                                    print(agregar_v(cod_v, nom, des, precio_v))
                                elif op == 2:
                                    codigo = input("Digite el codigo de la vacuna que desea eliminar: ")
                                    print(eliminar_v(codigo))
                                else:
                                    print("Opcion no valida, intente de nuevo")
                        elif op == 2: #tratamientos cod_t,nom,des,precio_t
                            while True:
                                op = int(input(sub_menu_ingreso))

                                if op == 3:
                                    break
                                elif op == 1:
                                    cod_t = input("Digite el codigo del tratamiento nuevo: ")
                                    nom = input("Digite el nombre del tratamiento nuevo: ")
                                    des = input("Digite una descripcion: ")
                                    precio_t = input("Digite el precio del tratamiento nuevo: ")
                                    print(agregar_t(cod_t, nom, des, precio_t))
                                elif op == 2:
                                    codigo = input("Digite el codigo del tratamiento que desea eliminar: ")
                                    print(eliminar_t(codigo))

                                else:
                                    print("------> Opcion no valida, intente de nuevo <-------")

                        elif op == 3: #servicios cod_s,nom,des,precio_s
                            while True:
                                op = int(input(sub_menu_ingreso))

                                if op == 3:
                                    break
                                elif op == 1:
                                    cod_s = input("Digite el codigo del servicio nuevo: ")
                                    nom = input("Digite el nombre del servicio nuevo: ")
                                    des = input("Digite una descripcion: ")
                                    precio_s = input("Digite el precio del servicio nuevo: ")
                                    print(agregar_s(cod_s, nom, des, precio_s))
                                elif op == 2:
                                    codigo = input("Digite el codigo del servicio que desea eliminar: ")
                                    print(eliminar_s(codigo))
                                else:
                                    print("------------> Opcion no valida, intente de nuevo <----------")

                        elif op == 4: #tipos de animales
                            while True:
                                op = int(input(sub_menu_ingreso))

                                if op == 3:
                                    break
                                elif op == 1:
                                    mascota = input("Digite el tipo que quiere agregar: ")
                                    print(c_mascota(mascota))
                                elif op == 2:
                                    codigo = input("Digite el nombre de la mascota que desea sacar del sistema: ")
                                    print(e_tipo(codigo))
                                else:
                                    print("-------> Opcion no valida, intente de nuevo <----------")

                        elif op == 5: #razas
                            while True:
                                op = int(input(sub_menu_ingreso))

                                if op == 3:
                                    break
                                elif op == 1:
                                    print(l_tipo())
                                    tipo = int(input("Seleccione la Mascota a la que quiere agregarle una raza: "))
                                    print(s_tipo(tipo))
                                    tip = s_tipo(tipo)
                                    raza = input("Digite la nueva raza que quiere agregar: ")

                                    print(c_raza(raza,tip))
                                elif op == 2:
                                    print(l_tipo())
                                    tipo = int(input("Seleccione la Mascota a la que quiere eliminarle una raza: "))
                                    print(s_tipo(tipo))
                                    tip = s_tipo(tipo)
                                    codigo = input("Digite el nombre de la raza que desea eliminar: ")
                                    print(e_raza(tip,codigo))
                                else:
                                    print("-------> Opcion no valida, intente de nuevo <--------")
                        else:
                            print("------> Opcion no valida, intente de nuevo <-----------")

                elif op == 5: #historial clinico
                    while True:
                        op =int(input(sub_menu_historial))
                        if op == 3:
                            break
                        elif op == 1: #cod_h,cod_m,peso,temperatura,tratamiento,observaciones,antecedentes
                            cod_h = input("Digite el codigo de el Historial: ")
                            print(l_mascotas())
                            p_cod_m = int(input("Seleccione la mascota que desea: "))
                            cod_m = consultar_cod_m(p_cod_m)
                            peso = 1 #arreglar
                            temp = int(input("Digite la temperatura del animal: "))
                            print(lt_servicios())
                            p_l_todo = int(input("seleccione que tratamiento/servicio/vacuna se utilizo en el animal: "))
                            l_todo = consultar_l_todo(p_l_todo)
                            observaciones = input("Digite las observacion: ")
                            antecedentes = input("Digite los antecedentes: ")
                            fecha = time.strftime(("%d/%m/%y"))
                            historial1 = Historial(cod_h,cod_m,peso,temp,l_todo,observaciones,antecedentes,fecha)
                            print(s_historial(cod_m,historial1))
                        elif op == 2:
                            print(l_mascotas())
                            s_mascota = int(input("Seleccione la mascota: "))
                            x = s_mascotas(s_mascota)
                            print(v_historial(x))

                elif op == 6: #control de vacunas
                    while True:
                        op = int(input(sub_menu_control))
                        if op == 3:
                            break
                        elif op == 1:
                            cod_c = input("Digite el codigo de control: ")
                            print(l_mascotas())
                            cod_m = input("Seleccione el codigo de la mascota: ")
                            print(l_vacunas())
                            cod_v = input("Seleccione el codigo de la vacuna: ")
                            f_aplicacion = input("Digite la fecha de aplicacion: ")
                            observaciones = input("Escriba las observaciones realizadas a la mascota: ")
                            control1 = Control(cod_c,cod_m,cod_v,f_aplicacion,observaciones)
                            print(control_v(cod_c,control1))
                        elif op == 2:
                            print(l_mascotas())
                            s_mascota = int(input("Seleccione la mascota: "))
                            i = s_mascotas(s_mascota)
                            print(v_control(i))


                elif op == 7:
                    while True:
                        op = int(input(sub_menu_registros))
                        if op == 6:
                            break
                        elif op == 1:
                            print(l_clientes())
                            cliente = int(input("Seleccione el cliente: "))
                            client = s_cliente(cliente)
                            print(client)
                        elif op == 2:
                            print(l_mascotas())
                            mascota = int(input("Seleccione la mascota: "))
                            print(c_mascotas(mascota))
                        elif op == 3: #vacunas
                            print(l_vacunas())
                            vacuna = int(input("Seleccione la vacuna: "))
                            vacuna = s_vacuna(vacuna)
                            print(vacuna)
                        elif op == 4: #tratamientos
                            print(l_tratamintos())
                            tratamiento = int(input("Seleccione el tratamiento: "))
                            tratamiento = s_tratamiento(tratamiento)
                            print(tratamiento)
                        elif op == 5: #servicios
                            print(l_servicios())
                            servicio = int(input("Seleccione el servicio: "))
                            servicio = s_servicio(servicio)
                            print(servicio)
                elif op == 8:
                    while True:
                        op =int(input(sub_menu_servicios))
                        if op ==4:
                            break
                        elif op ==1:
                            pass
                        elif op ==2:
                            pass
                        elif op ==3:
                            pass
                elif op == 9:
                    print(lt_servicios())

                    servi = int(input("Seleccione el servicio deseado: "))
                elif op == 10: #pagos
                    pass
                else:
                    print("------> Opcion no valida, intente de nuevo <-----------")
        else:
            print(inicio(usua,password))
    else:
        print("----------> Opcion no valida, intente de nuevo <--------")